import Swal from 'sweetalert2';

window.body = window.$('body');

window.ajaxFormSubmit = function ajaxFormSubmit(
    url,
    method,
    action,
    formData,
    type,
    notificationTimer = 5000
) {
    window.$.ajax({
        url: url,
        type: method,
        headers: {
            'X-CSRF-TOKEN': window.$('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        processData: false,
        contentType: false,
        data: formData,
        beforeSend: function () {
            window.riftWebFormApiBeforeSendCallback?.();

            window.$('.ajaxFormError').remove();
        },
        success: function (response) {
            if (type === 'auth') {
                location.reload();
            }

            if (response.status === 'success' || response.status === 'true' || response.status === true) {
                window.riftWebFormApiSuccessCallback?.(response);

                if (response.data.update && response.data.update.length > 0) {
                    window.$.each(response.data.update, function (i, v) {
                        let el = window.$('#' + i);
                        if (el.length > 0) {
                            el.html(v);
                        }
                    })
                }

                if (response.data.scroll && response.data.scroll.length > 0) {
                    window.$('html, body').animate({
                        scrollTop: window.$('#' + response.data.scroll).offset().top
                    });
                }

                if (typeof DataTable !== 'undefined' && response.data.datatable && response.data.datatable.length > 0) {
                    window.$.each(response.data.datatable, function (i, v) {
                        window.$('#' + v).DataTable().ajax.reload();
                    });
                }

                if (response.data.remove && response.data.remove.length > 0) {
                    window.$.each(response.data.remove, function (i, v) {
                        window.$('#' + v).remove();
                    })
                }

                if (response.data.redirect && response.data.redirect.length > 0) {
                    setTimeout(function () {
                        if (response.data.redirect === 'refresh') {
                            location.reload()
                            return;
                        }

                        window.location.href = response.data.redirect;
                    }, notificationTimer);
                }

                if (typeof window.$.fn.modal === 'function') {
                    window.$('.modal').modal('hide');
                }

                window.ajaxFormNotification(type, 'success', action, notificationTimer);
                return;
            }

            window.riftWebFormApiErrorCallback?.(response);

            window.ajaxFormNotification(type, 'error', action, notificationTimer);
        },
        error: function (err) {
            window.riftWebFormApiErrorCallback?.(err);

            window.ajaxFormNotification(type, 'error', action, notificationTimer);
            if (err?.status === 422) {
                window.$.each(err.responseJSON.errors, function (i, error) {
                    let el = window.$(document).find('[name="' + i + '"]');
                    el.after(window.$('<div class="ajaxFormError alert alert-danger mt-2"><span class="svg-icon svg-icon-danger svg-icon-2hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"><rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/><rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black"/><rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"/></svg></span><div class="d-flex flex-column"><h4 class="mb-1 text-dark">Alerta</h4><span>' + error[0].replace(i.replace('_', ' '), el.attr('data-title')) + '</span></div></div>'));
                });
            }
        },
        complete: function () {
            window.riftWebFormApiCompleteCallback?.();
        }
    });
}

window.formApiActionAlert = function formApiActionAlert(
    url,
    method,
    type,
    action,
    formData,
    notificationTimer = 2500
) {
    if (action !== 'update' && action !== 'delete') {
        ajaxFormSubmit(url, method, action, formData, type, notificationTimer);
        return;
    }

    const notificationParams = window.getNotificationTranslationParameters(type, action, 'warning');

    Swal.fire({
        icon: 'warning',
        title: notificationParams.title,
        text: notificationParams.description,
        showCancelButton: true,
        showCloseButton: false,
        confirmButtonText: notificationParams.button,
        cancelButtonText: notificationParams.cancel
    }).then((result) => {
        if (result.isConfirmed) {
            ajaxFormSubmit(url, method, action, formData, type, notificationTimer);
        }
    });
}

window.getNotificationTranslationParameters = function getNotificationTranslationParameters(
    type,
    action,
    status
) {

    if (
        typeof window?.language === 'undefined' ||
        typeof window?.language[type] === 'undefined' ||
        typeof window?.language[type]['notifications'] === 'undefined' ||
        typeof window?.language[type]['notifications'][action] === 'undefined' ||
        typeof window?.language[type]['notifications'][action][status] === 'undefined'
    ) {
        console.error('RIFT Form API: Translations not found [' + type + '][' + action + '][' + status + ']');
    }

    const title = window.language?.[type]?.notifications?.[action]?.[status]?.title ?? 'Translation not found';
    const description = window.language?.[type]?.notifications?.[action]?.[status]?.description ?? 'Translation not found';
    const button = window.language?.[type]?.notifications?.[action]?.[status]?.button ?? 'Translation not found';
    const cancel = window.language?.general?.cancel ?? 'Translation not found';

    let notificationParameters = {
        title: "<strong>" + title + "</strong>",
        description: description,
        button: button,
        cancel: cancel
    };

    if (status === 'success') {
        notificationParameters.icon = 'ci-check-circle';
        notificationParameters.type = 'success';
    } else {
        notificationParameters.icon = 'ci-close-circle';
        notificationParameters.type = 'danger';
    }

    return notificationParameters;
}

window.ajaxFormNotification = function ajaxFormNotification(
    type,
    status,
    action,
    notificationTimer = 5000
) {
    const notificationParameters = getNotificationTranslationParameters(type, action, status);

    window.$.notify({
        icon: notificationParameters.icon,
        title: notificationParameters.title,
        message: "<br>" + notificationParameters.description
    }, {
        element: 'body',
        type: notificationParameters.type,
        animate: {
            enter: 'animated flipInY',
            exit: 'animated flipOutY'
        },
        placement: {
            from: 'bottom',
            align: 'right'
        },
        delay: notificationTimer
    });
}
body.on('submit', 'form.ajax', function (e) {
    e.preventDefault();

    // validate the form
    if (!this.checkValidity()) {
        console.log('RIFT Form API: Form is invalid');
        if (typeof window.riftWebFormApiErrorCallback === 'function') {
            window.riftWebFormApiErrorCallback();
        }
        return;
    }

    if (typeof tinymce !== 'undefined') {
        tinymce.triggerSave();
    }

    const url = window.$(this).attr('action');
    const method = window.$(this).attr('method');
    const action = window.$(this).attr('data-action');
    const type = window.$(this).attr('data-type');
    let formData = new FormData(window.$(this).get(0));

    /* Special Stuff */
    window.$(this).find('[data-ckeditor]').each(function () {
        formData.append(window.$(this).attr('name'), window.$('div[data-id=' + window.$(this).attr('data-ckeditor') + ']').html())
    });

    formApiActionAlert(
        url,
        method,
        type,
        action,
        formData,
        2500
    );
});